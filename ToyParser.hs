-- Grammar in EBNF:
-- ExprAdd ::= ExprMult {"+" ExprMult}
-- ExprMult ::= AtomicExpr {"*" AtomicExpr}
-- AtomicExpr ::= Number | "(" ExprAdd ")"

-- Grammar in strict LL(1) form (the code follows this fashion)
-- ExprAdd ::= ExprMult RestExprAdd
-- RestExprAdd ::= epsilon | "+" ExprMult RestExprAdd
-- ExprMult ::= AtomicExpr RestExprMult
-- RestExprMult ::= epsilon | "*" AtomicExpr RestExprMult
-- AtomicExpr ::= Number | "(" ExprAdd ")"

data Token
    = Number Integer
    | OpenPar  -- opening parenthesis
    | ClosePar
    | Plus
    | Times
    deriving (Eq, Show)

data Expression
    = Add Expression Expression
    | Mult Expression Expression
    | Val Integer
    deriving Show

type Parser a = [Token] -> Maybe (a, [Token])

exprAdd :: Parser Expression
exprAdd ts1 = do
    (e, ts2) <- exprMult ts1
    (es, ts3) <- restExprAdd ts2
    return (foldl Add e es, ts3)

restExprAdd :: Parser [Expression]
restExprAdd (Plus : ts1) = do
    (e, ts2) <- exprMult ts1
    (es, ts3) <- restExprAdd ts2
    return (e:es, ts3)
restExprAdd ts = return ([], ts)

exprMult :: Parser Expression
exprMult ts1 = do
    (e, ts2) <- atomicExpr ts1
    (es, ts3) <- restExprMult ts2
    return (foldl Mult e es, ts3)

restExprMult :: Parser [Expression]
restExprMult (Times : ts1) = do
    (e, ts2) <- atomicExpr ts1
    (es, ts3) <- restExprMult ts2
    return (e:es, ts3)
restExprMult ts = return ([], ts)

atomicExpr :: Parser Expression
atomicExpr (Number i : ts) = return (Val i, ts)
atomicExpr (OpenPar : ts1) = do
    (e, ts2) <- exprAdd ts1
    case ts2 of
        ClosePar : ts3 -> return (e, ts3)
        _ -> Nothing
atomicExpr _ = Nothing

-- (1 + 2) + 2 * 3
example = exprAdd [OpenPar, Number 1, Plus, Number 2, ClosePar, Plus, Number 2, Times, Number 3]